# xfwm4-theme-smallscreen-raleigh-compact

An Xfwm4 theme based on the "smallscreen" theme [0], colours adapted to fit the 'raleigh-reloaded' [1] theme, tweaked to use only necessary screen space.

License: GPL3 or later [2].

## Installation:

* To install as user, copy [`themes/smallscreen-raleigh-compact/xfwm4`](themes/smallscreen-raleigh-compact/) into `${HOME}/.themes/smallscreen-raleigh-compact/`.
* To install systemwide, copy [`themes/smallscreen-raleigh-compact/xfwm4`](themes/smallscreen-raleigh-compact/) into `/usr/share/themes/smallscreen-raleigh-compact/`.
* On Arch Linux based systems, the AUR package [`xfwm4-theme-smallscreen-raleigh-compact-git`](https://aur.archlinux.org/packages/xfwm4-theme-smallscreen-raleigh-compact-git) installs this theme systemwide.

## Screenshot(s):

![Screenshot of Xfwm4 theme 'smallscreen-raleigh-compact'](screenshot.png "screenshot.png").

---

[0] https://archive.xfce.org/src/art/xfwm4-themes/4.10/xfwm4-themes-4.10.0.tar.bz2, retrieved: 2023-07-18.  
[1] https://github.com/vlastavesely/raleigh-reloaded, retrieved: 2023-07-18.  
[2] https://www.gnu.org/licenses/gpl-3.0.en.html, checked: 2023-07-19. License text: [`COPYING.md`](COPYING.md) in this repository, retrieved: 2023-07-19.
